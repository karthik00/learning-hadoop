import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.*;

public class MapTest {

    private Configuration configuration;
    private HadoopConnection connection;
    private DistributedFileSystem distributedFileSystem;

    @Before
    public void setUp() throws Exception {
        configuration = new Configuration();
        configuration.set("fs.default.name", "hdfs://10.4.3.193:9000");
        distributedFileSystem = (DistributedFileSystem) FileSystem.get(configuration);
        connection = HadoopConnection.connect(configuration, distributedFileSystem);

    }

    @Test
    public void shouldConnectToDFS() {
        assertNotNull(connection);
        assertEquals("hdfs://10.4.3.193:9000", connection.connectionURI());

    }

    @Test
    public void shouldGetFileSystemInstance() {
        assertNotNull(connection.getDFS());
    }

    @Test
    public void shouldCreateAndDeleteFile() {
        FSDataOutputStream dataOutputStream = connection.createFile(new Path("hdfs://10.4.3.193:9000/tests/test"));
        assertNotNull(dataOutputStream);
        try {
            dataOutputStream.writeUTF("Testing");
        } catch (IOException e) {
            fail();
        }
    }


}
