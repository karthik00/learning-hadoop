import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.DistributedFileSystem;

import java.io.IOException;

public class HadoopConnection {
    Configuration configuration;
    private DistributedFileSystem distributedFileSystem;

    public HadoopConnection(Configuration configuration,DistributedFileSystem distributedFileSystem) {
        this.configuration = configuration;
        this.distributedFileSystem = distributedFileSystem;
    }

    public static HadoopConnection connect(Configuration configuration,DistributedFileSystem distributedFileSystem) {
        return new HadoopConnection(configuration,distributedFileSystem);
    }

    public String connectionURI() {
        return configuration.get("fs.default.name");
    }

    public DistributedFileSystem getDFS() {
        return distributedFileSystem;
    }

    public FSDataOutputStream createFile(Path path) {
        try {
            return this.getDFS().create(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
